%define	name		libwcat
%define	version		1.1
%define	release		1mdk
%define	major		1
%define libname	%mklibname wcat %{major}

Summary:		Library for the watchcat software watchdog
Name:			%{name}
Version:		%{version}
Release:		%{release}
URL:			http://oss.digirati.com.br/watchcatd/
License:		LGPL
Source0:		%{name}-%{version}.tar.bz2
Group:			System/Libraries
Obsoletes:		%{libname}  = %{version}
Obsoletes:		%{name}
Provides:		%{libname} = %{version}
Provides:		%{name}
BuildRoot:		%{_tmppath}/%{name}-%{version}-root

%description
libwcat is an API to watchcatd, a software watchdog that uses an
approach not as drastic as the usual watchdog solutions. It tries
to kill the locked process only.

%package -n		%{libname}
Summary:		Library for the watchcat software watchdog
Group:          System/Libraries
Obsoletes:		%{libname} = %{version}
Obsoletes:		%{name}
Provides:		%{libname} = %{version}
Provides:		%{name}

%description -n		%{libname}
libwcat is an API to watchcatd, a software watchdog that uses an
approach not as drastic as the usual watchdog solutions. It tries
to kill the locked process only.

%package -n		%{libname}-devel
Summary:		Static library and header files for the watchcat library
Group:			Development/C
License: 		LGPL
Obsoletes:		%{libname}-devel  = %{version}
Obsoletes:		%{name}-devel
Provides:		%{libname}-devel = %{version}
Provides:		%{name}-devel
Requires:		%{libname} = %{version}-%{release}

%description -n		%{libname}-devel
libwcat is an API to watchcatd, a software watchdog that uses an
approach not as drastic as the usual watchdog solutions. It tries
to kill the locked process only.

This package contains the static libwcat library and its header files
needed to compile applications that use libwcat.

%prep
%setup -q -n %{name}-%{version}

%build
export CFLAGS="%{optflags}"
%make 

%install
strip %{name}.so.%{major}.%{version}
%__rm -rf %buildroot
install -d %{buildroot}/%{_includedir}
install -d %{buildroot}/%{_libdir}
install -m755 %{name}.so.%{major}.%{version} %{buildroot}/%{_libdir}/
install -m644 %{name}.a %{buildroot}/%{_libdir}/
install -m644 watchcat.h %{buildroot}/%{_includedir}/

ln -s %{name}.so.%{major}.%{version} %{buildroot}%{_libdir}/%{name}.so
ln -s %{name}.so.%{major}.%{version} %{buildroot}%{_libdir}/%{name}.so.%{major}

%clean
%__rm -rf %buildroot

%post -n %{libname} -p /sbin/ldconfig
%postun -n %{libname} -p /sbin/ldconfig

%post -n %{libname}-devel -p /sbin/ldconfig
%postun -n %{libname}-devel -p /sbin/ldconfig

%files -n %{libname}
%defattr(-,root,root)
%{_libdir}/*.so.*

%files -n %{libname}-devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/*.a

%changelog
* Wed Aug 27 2008 Andre Nathan <andre@digirati.com.br> 1.1-1mdk
- Version 1.1.
* Fri Feb 06 2004 Michel Machado <michel@digirati.com.br> 1.0-1mdk
- Minor fixes.
* Thu Feb 05 2004 Andre Nathan <andre@digirati.com.br> 0.1-2mdk
- Export CFLAGS before building;
* Fri Jan 23 2004 Andre Nathan <andre@digirati.com.br> 0.1-1mdk
- First version of the .spec.
