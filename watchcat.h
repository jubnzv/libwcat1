/*
** $Id: watchcat.h 1654 2006-05-29 18:18:02Z andre $
** libwcat - Watchcat Client Library
** See copyright notice in distro's COPYRIGHT file
*/

#ifndef WATCHCAT_H
#define WATCHCAT_H

#include <signal.h>

int cat_open(void);
int cat_open1(int timeout, int signal, const char *info);
int cat_heartbeat(int cat);
int cat_close(int cat);

/* Debug purpose only. Please, don't use it in prodution code. */
#define CAT_DEVICE_SIZE 128
void cat_set_device(const char *dev);

#endif
